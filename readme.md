# TP génération de tubes

Le but de ce TP est lé génération procédurale de tubes. Cette génération se fera
en plusieurs étapes.

1. [Génération d'une courbe pour l'axe du tube](#axe)
1. [Subdivision de la courbe pour la lisser](#subdivision)
1. [Génération d'un repère le long de la courbe](#repere)
1. [Génération de cercles autour de la courbe](#circles)
1. [Génération de portions de cylindres](#cylindre)

<a name="axe"></a>
## Génération de l'axe du tube

[[Démo de l'objectif de cette partie]](https://perso.liris.cnrs.fr/vincent.nivoliers/mif27/curve_generation/initial_curve/)

Le but de cette partie est de générer une séquence de points dans l'espace qui
formeront la base de l'axe de votre tube. Commencez par une méthode simple (un
nombre fini de points fixés), que vous améliorerez par la suite si vous avez le
temps. Vous pourrez ainsi garder du temps pour aborder la suite de ce TP et
demander conseil à votre encadrant. Pour stocker ces points vous pouvez utiliser
le type
[Point](http://perso.univ-lyon1.fr/jean-claude.iehl/Public/educ/M1IMAGE/html/structPoint.html),
et placer ces points dans un `std::vector` ou ce que bon vous semble.

Pour visualiser votre séquence de points reliés par des lignes, vous pouvez
créer un maillage
([Mesh](http://perso.univ-lyon1.fr/jean-claude.iehl/Public/educ/M1IMAGE/html/mesh_8h.html))
de type `GL_LINES` et y ajouter vos sommets. Pour le remplir, pour chaque arête
que vous souhaitez afficher, ajoutez ses deux sommets au maillage. Par exemple,
pour créer la ligne brisée constituée des points $`(1,0,0)`$, $`(0,1,0)`$ et
$`(0,0,1)`$, vous utiliserez un code de la forme :

```C++
Mesh line(GL_LINES) ;
line.vertex(1., 0., 0.) ;
line.vertex(0., 1., 0.) ;
line.vertex(0., 1., 0.) ; //ce sommet est spécifié une fois par arête
line.vertex(0., 0., 1.) ;
```

<a name="subdivision"></a>
## Subdivision de la courbe

[[Démo de l'objectif de cette partie]](https://perso.liris.cnrs.fr/vincent.nivoliers/mif27/curve_generation/subdivided/)

Pour l'instant votre courbe est tout en angles, mais vous aimeriez la rendre
plus lisse. Vous pouvez pour cela utiliser le mécanisme de la subdivision. La
subdivision a été mise au point au départ comme une méthodes efficace pour
tracer des courbes. Dans votre cas nous vous proposons la méthode de Chaikin,
aussi appelé "découpe des coins", qui permet de raffiner votre ligne brisée et
la faire tendre vers une suite de paraboles $`x \mapsto x^2`$ dont les
extrémités sont alignées entre elles.

![chaikin](Images/chaikin.png)

Le schéma de subdivision de Chaikin consiste à introduire deux points sur chaque
segment de votre ligne brisée : le premier à $`\frac{1}{4}`$ et le second à
$`\frac{3}{4} `$, comme sur l'image ci dessus. Oubliez ensuite les sommets
initiaux. Le plus simple consiste donc à créer vos points dans un nouveau
tableau. En itérant le processus plusieurs fois, vous doublez grossièrement le
nombre de points à chaque passe et votre courbe devient plus lisse. 

Pour calculer les positions de vos points, vous pouvez calculer les coordonnées
à la main, ou utiliser la classe 
[Vector](http://perso.univ-lyon1.fr/jean-claude.iehl/Public/educ/M1IMAGE/html/structVector.html).
Un vecteur peut être créé à partir de deux points `a` et `b` via l'opération
`b-a`. Vous obtenez ainsi le vecteur qui permet de translater `a` sur `b`. Il 
est ensuite possible de multiplier ce vecteur par une constante et de l'ajouter
à un point pour le translater. Via cette méthode, le milieu du segment `[a,b]`
serait obtenu via

```C++
Point m = a + 0.5*(b-a) ;
```

La courbe limite de ce schéma est $`\mathcal{C}^1`$, ce qui signifie que la
courbe n'a pas de trous et que la tangente est définie et continue partout (pas
d'angles).  Cette courbe limite s'appelle une
[B-spline](https://fr.wikipedia.org/wiki/B-spline), et il est possible de
définir simplement d'autres schémas de subdivision pour augmenter ce degré de
continuité.


<a name="repere"></a>
## Génération d'un repère le long de la courbe

[[Démo de l'objectif de cette partie]](https://perso.liris.cnrs.fr/vincent.nivoliers/mif27/curve_generation/frames/)

Pour pouvoir placer des objets le long de votre courbe (par exemple un véhicule
ou une caméra), il est nécessaire de définir un repère en chaque point de la
courbe, et de s'assurer que ces repères sont cohérents (pas de rotation
brusque). 

En 3D, un repère se matérialise par un centre et un groupe de trois vecteurs de
norme 1 mutuellement orthogonaux. Dans votre cas, le centre est clair (le point
de la courbe), et vous disposez d'une direction : celle de votre courbe. Vous
pouvez normaliser un vecteur en utilisant la fonction
[normalize](http://perso.univ-lyon1.fr/jean-claude.iehl/Public/educ/M1IMAGE/html/group__math.html#gae536ea1a67e0b896ceebfeff08b18d3d).
Étant donné deux vecteurs, le produit vectoriel (
[cross](http://perso.univ-lyon1.fr/jean-claude.iehl/Public/educ/M1IMAGE/html/group__math.html#gaf70e8601a09ec83cf6748657fae04883))
permet d'en obtenir un troisième orthogonal à ces deux vecteurs. Il vous suffit
donc de générer un vecteur orthogonal à l'axe de votre courbe, et vous pourrez
facilement calculer le troisième. Le point compliqué est de faire en sorte que
ces vecteurs restent grossièrement alignés entre eux le long de la courbe.

Pour chaque point de la courbe, vous aurez donc à stocker un vecteur orthogonal
au segment de courbe qui commence à ce point. Pour le dernier point, de la
courbe, vous utiliserez le même vecteur que le point précédent. N'hésitez pas à
utiliser un maillage de type `GL_LINES` pour visualiser vos vecteurs, comme
réalisé dans la démo.

### Un premier vecteur orthogonal à la courbe

Une méthode pour générer ces vecteurs orthogonaux le long de la courbe consiste
à en générer un premier sur le premier point de la courbe, et à l'utiliser pour
calculer les suivants un par un au fur et à mesure des nouveaux points de la
courbe.

La méthode est la suivante :

1. Calculez le vecteur $`\mathbf{a}`$ correspondant à l'axe de la courbe par
   la différence des sommets du segment.
1. Prenez un vecteur $`\mathbf{v}`$, n'importe lequel pourvu qu'il ne soit pas
   aligné avec le premier segment de la courbe (voir point suivant). Par exemple
   utilisez le vecteur $`(1,0,0)`$ et s'il est aligné, alors utilisez le
   vecteur $`(0,1,0)`$.
1. Calculez le produit vectoriel de $`\mathbf{a}`$ et de $`\mathbf{v}`$. Si
   le vecteur obtenu est nul, c'est que les deux vecteurs sont alignés. Sinon
   vous avez obtenu un vecteur $`\mathbf{d}`$ orthogonal à $`\mathbf{a}`$ et
   $`\mathbf{v}`$, et donc en particulier à $`\mathbf{a}`$.
1. Normalisez le vecteur obtenu.

### Déplacer ce premier vecteur le long de la courbe

Nous allons maintenant propager ce premier vecteur orthogonal le long de la
courbe. Étant donné un segment $`\mathbf{a_0}`$ pour lequel le vecteur
orthogonal $`\mathbf{d}`$ est calculé, et si $`\mathbf{a_1}`$ est l'axe
suivant, nous allons calculer la rotation qui permet de transformer
$`\mathbf{a_0}`$ en $`\mathbf{a_1}`$ et l'appliquer à $`\mathbf{d}`$.
[Une recherche avec les bons mots
clé](https://duckduckgo.com/?q=rotation+between+two+vectors+3D&ia=qa) vous
permettra de trouver comment calculer cette rotation. Dans la réponse la plus
pertinente, le produit vectoriel est noté $`\mathbf{a} \times \mathbf{a}`$ et
$`(v_1, v_2, v_3)`$ sont les coordonnées du vecteur $`\mathbf{v}`$.

Vous pourrez utiliser la classe 
[Transform](http://perso.univ-lyon1.fr/jean-claude.iehl/Public/educ/M1IMAGE/html/mat_8h.html)
pour représenter votre transformation. Cette classe ne vous permet cependant pas
de réaliser la somme de deux matrices. Vous pouvez néanmoins utiliser le code
suivant pour spécifier votre matrice coefficient par coefficient.

```C++
Transform t ;
for(int i  = 0; i < 4; ++i) {
  for(int j = 0; j < 4; ++j) {
    t.m[i][j] = ... ;
  }
}
```

Notez qu'il s'agit d'une matrice 4x4, mais que dans votre cas, la dernière ligne
et la dernière colonne sont nulles, à part un 1 sur la diagonale.

<a name="circles"></a>
## Génération de cercles autour de la courbe

[[Démo de l'objectif de cette partie]](https://perso.liris.cnrs.fr/vincent.nivoliers/mif27/curve_generation/circles/)

Vous avez désormais en chaque point de la courbe un vecteur orthogonal cohérent
le long de la courbe. Vous pouvez vous en servir pout générer des cercles autour
de la courbe. Une méthode consiste à créer une matrice de rotation ayant pour
axe l'axe de la courbe, et pour angle un angle permettant de générer autant de
points que désiré le long du cercle. Muni de cette transformation, vous prendre
une copie de votre vecteur orthogonal et itérativement le faire tourner via
votre matrice. À chaque itération, vous obtenez un point du cercle en
translatant le point de l'axe de la courbe le long de ce vecteur.

Pour générer votre matrice de rotation, la classe
[Transform](http://perso.univ-lyon1.fr/jean-claude.iehl/Public/educ/M1IMAGE/html/mat_8h.html)
est munie de constructeurs tout à fait adaptés à ce que vous cherchez.

<a name="cylindre"></a>
## Génération de portions de cylindre

[[Démo de l'objectif de cette partie]](https://perso.liris.cnrs.fr/vincent.nivoliers/mif27/curve_generation/extruded/)

Vous pouvez maintenant relier entre eux les sommets de vos cercles par des
triangles, vous avez votre tube !

<a name="time"></a>
## Pour aller plus loin

[[Démo de l'objectif de cette partie]](https://perso.liris.cnrs.fr/vincent.nivoliers/mif27/curve_generation/noodles/)

Il est maintenant temps de travailler sur la méthode de génération de votre
courbe initiale pour la rendre conforme à vos souhaits. Vous pouvez également
utiliser `SDL_GetTicks()` pour la faire évoluer avec le temps.
